jQuery(document).ready(function($){
	"use strict";
  //StickyNav
  $(window).scroll(function(){
      if ($(window).scrollTop() >= 80) {
          $('#stickyNav').css("top","0");
      }
      else {
          $('#stickyNav').css("top","-70px");
      }
  });
//Scroll to top
$('#bckToTop').click(function(){
    $("html, body").scrollTop(0);
        return false;
});
//Word Ticker
$(function () {
  var count = 0;
  var wordsArray = ["expense reports", "chasing receipts", "reimbursements"];
  setInterval(function () {
    $("#wordTicker").fadeOut(400, function () {
      $(this).text(wordsArray[count % wordsArray.length]).fadeIn(400);
    });
    count++;
  }, 2000);
});
//Tabs
$("button.tablinks").click(function(){
  var elementId = $(this).attr('id');
  $("button.tablinks").removeClass('active');
  $(this).addClass('active');
  $('.tabcontent').fadeOut();
  $('div#'+elementId).fadeIn();
  $(this).first('.accordion').css("color", "#383E66");
  $(this).first('.panel').show();
});
//Tabs Accordion
$("button.accordion").click(function(){
  $('.accordion').css("color", "#CDCED9");
  $(this).css("color", "#383E66");
  $(this).next('.panel').slideToggle();
  $(this).next('.panel').siblings('.panel').slideUp();
});
//Tooltips
$( ".questionmark" ).hover(function() {
	event.stopPropagation();     
    if($(this).siblings('.tooltiptext:visible').length){
        $(this).siblings('.tooltiptext').hide();
	}else{
		$('.tooltiptext').hide();
        $(this).siblings('.tooltiptext').show();        
	}
});
//Hide tooltips when we click on body
$(document).click( function(){
		$('.tooltiptext').hide();
	});
//FAQ
$('.accordionFaq').click(function(event){ 
  $(this).find('.horizontal').toggleClass('rotated180');
  $(this).find('.vertical').toggleClass('rotated90');
  $(this).next('.panelFaq').slideToggle();
  $(this).next('.panelFaq').siblings('.panelFaq').slideUp();
});    
 //burger
 $(".burger").click(function(){
  $(".bar1").toggleClass("change");
});   
  $(".burger").click(function(){
  $(".bar2").toggleClass("change");
});  
    $(".burger").click(function(){
  $(".bar3").toggleClass("change");
});
    
});
